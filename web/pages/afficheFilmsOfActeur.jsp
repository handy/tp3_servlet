<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>filmographie</title>
</head>
<body>
<h1>Liste des Films de l'acteur : </h1>
<c:forEach var="film" items="${ensFilms}">
    <p>${film.titre}, ${film.nom_realisateur}}  | <a href="processActeur?action=infoFilm&nfilm=${film.nfilm}">voir plus</a></p>
</c:forEach>

</body>
</html>
