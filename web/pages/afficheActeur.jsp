<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Acteurs</title>
</head>
<body>
    <h1>Liste de tout les acteurs :</h1>
    <c:forEach var="acteur" items="${ensActeur}">
        <p>${acteur.nom}, ${acteur.prenom}  | <a href="processActeur?action=carriere&nacteur=${acteur.nActeur}">voir plus</a></p>
    </c:forEach>

</body>
</html>
