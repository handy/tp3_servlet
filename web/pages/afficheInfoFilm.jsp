
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>${film.titre}</title>
</head>
<body>
    <h1>${film.titre}</h1>
    <h3>${film.nom_realisateur}, ${film.genre}, ${film.sortie}, ${film.pays}</h3>
    <p>entrées : ${film.entrees}</p>
</body>
</html>
