package control;

import dao.CinemaDAO;
import dto.ActeurDTO;
import dto.FilmDTO;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@WebServlet("/processActeur")
public class CarriereServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try{
            String action = request.getParameter("action");
            //Execution du traitement associe a cette actin
            switch (action) {
                case "init":
                    doInit(request, response);
                    break;
                case "carriere":
                    doCarriere(request, response);
                    break;
                case "infoFilm":
                    doInfoFilms(request, response);
                    break;
                default: doInit(request, response);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void forward(String url, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = request.getRequestDispatcher(url);
        requestDispatcher.forward(request, response);
    }

    private void doInit(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if (request.getSession().getAttribute("ensActeur")==null){
            List<ActeurDTO> ensActeur = CinemaDAO.getInstance().ensActeur();

            request.getSession().setAttribute("ensActeur", ensActeur);


        }
        String url = "pages/afficheActeur.jsp";
        forward(url, request, response);
    }

    protected void doCarriere(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String choix = request.getParameter("nacteur");
        int nacteur = Integer.parseInt(choix);


            List<FilmDTO> ensFilms = CinemaDAO.getInstance().ensfilmOfActeur(nacteur);

            request.setAttribute("ensFilms", ensFilms);

        String url = "pages/afficheFilmsOfActeur.jsp";
        forward(url, request, response);
    }

    protected void doInfoFilms(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String choix = request.getParameter("nfilm");
        int nfilm = Integer.parseInt(choix);


            FilmDTO film = CinemaDAO.getInstance().infoRealisateurEtActeur(nfilm);

            request.setAttribute("film", film);

        String url = "pages/afficheInfoFilm.jsp";
        forward(url, request, response);
    }
}
