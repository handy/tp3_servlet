package dao;

import dto.ActeurDTO;
import dto.FilmDTO;
import dto.GenreDTO;
import service.ConnectCinema;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class CinemaDAO {
    private final static String SQLFindGenreByID =
            "SELECT nature " +
                    "FROM GENRE " +
                    "WHERE ngenre = ?";

    private final static String SQLFindAllGenre =
            "SELECT ngenre, nature "
                    + "FROM GENRE "
                    + "ORDER BY nature";

    private final static String SQLFindNbFilmByGenre =
            "SELECT nature, count(nfilm) as nbr "
                    + "FROM FILM f, GENRE g "
                    + "WHERE f.ngenre = g.ngenre "
                    + "AND f.ngenre = ?";

    private final static String SQLFindAllFilmByGenre =
            "SELECT f.nfilm, f.titre, f.realisateur, a.nom "
                    + "FROM FILM f, GENRE g, ACTEUR a "
                    + "WHERE f.ngenre = g.ngenre "
                    + "AND f.ngenre = ? "
                    + "AND f.nacteurPrincipal = a.nacteur "
                    + "ORDER BY f.titre ";

    private final static String SQLFindFilmById =
            "select nfilm, titre, realisateur, nature, entrees, p.nom as pays, sortie " +
                    "from FILM f " +
                    "join GENRE g using(ngenre) " +
                    "join PAYS p on f.npays = p.npays " +
                    "where nfilm = ?";

    private final static String SQLFindAllActeur =
            "SELECT nacteur, nom, prenom FROM ACTEUR ORDER BY nom";

    private final static String SQLFindFilmsOfActeur =
            "SELECT nfilm, titre, realisateur, nature " +
                    "FROM FILM " +
                    "join GENRE using(ngenre) " +
                    "where nacteurPrincipal =? " +
                    "order by titre";

    private final static String SQLFindActeurById =
            "SELECT nom , prenom " +
                    "FROM ACTEUR " +
                    "WHERE nacteur = ? " +
                    "order by nom";



    //SINGLETON precoce
    private static CinemaDAO INSTANCE = new CinemaDAO();

    public static CinemaDAO getInstance() {
        return INSTANCE;
    }

    public GenreDTO findGenreById(int ngenre) {
        GenreDTO genre = null;

        try {
            PreparedStatement pstr =
                    ConnectCinema.getInstance().prepareStatement(SQLFindGenreByID);
            pstr.setInt(1, ngenre);
            ResultSet rs = pstr.executeQuery();

            if (rs.next()) {
                String nature = rs.getString(1);
                genre = new GenreDTO(ngenre, nature);
            }


        } catch (SQLException e) {
            e.printStackTrace();
        }
        return genre;
    }

        public List<GenreDTO> ensGenres(){
        List<GenreDTO> liste = new ArrayList<>();
        try {
            Statement instr =
                    ConnectCinema.getInstance().createStatement();
            ResultSet rs = instr.executeQuery(SQLFindAllGenre);

            while (rs.next()) {
                int ngenre = rs.getInt(1);
                String nature = rs.getString("NATURE");

                liste.add(new GenreDTO(ngenre, nature));
            }
        } catch (SQLException e) {
            //TODO Auto-generated catch block
            e.printStackTrace();
        }
        return liste;
    }

    public long nbreFilmDuGenre(int unGenre) {
        long resultat = -1L;
        try {
            Connection conn = ConnectCinema.getInstance();
            PreparedStatement pstm = conn.prepareStatement(SQLFindNbFilmByGenre);
            pstm.setInt(1, unGenre);
            ResultSet rs = pstm.executeQuery();
            if (rs.next()) {
                resultat = rs.getLong("nbr");
            }
        } catch (SQLException e) {
            //TODO Auto-generated catch block
            e.printStackTrace();
        }
        return resultat;
    }

    public List<FilmDTO> ensFilmDuGenre(int unGenre) {
        List<FilmDTO> liste = new ArrayList<>();
        try {
            Connection conn = ConnectCinema.getInstance();
            PreparedStatement pstm = conn.prepareStatement(SQLFindAllFilmByGenre);
            pstm.setInt(1, unGenre);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {

                int nfilm = rs.getInt("nfilm");
                String titre = rs.getString("titre");
                String nom_realisateur = rs.getString("realisateur");
                String nom_acteur = rs.getString("nom");

                liste.add(new FilmDTO(nfilm, titre, nom_realisateur));
            }
        } catch (SQLException e) {
            //TODO Auto-generated catch block
            e.printStackTrace();
        }
        return liste;
    }

    public FilmDTO infoRealisateurEtActeur(int unFilm) {
        FilmDTO film = null;
        try {
            Connection conn = ConnectCinema.getInstance();
            PreparedStatement pstm = conn.prepareStatement(SQLFindFilmById);
            pstm.setInt(1, unFilm);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {

                int nfilm = rs.getInt("nfilm");
                String titre = rs.getString("titre");
                String nom_realisateur = rs.getString("realisateur");
                String nature = rs.getString(4);
                int entrees = rs.getInt(5);
                String pays = rs.getString(6);
                String sortie = rs.getString(7);


                film = new FilmDTO(nfilm, titre, nom_realisateur, nature, entrees, sortie, pays );
            }
        } catch (
                SQLException e) {
            //TODO Auto-generated catch block
            e.printStackTrace();
        }
        return film;
    }

    public List<ActeurDTO> ensActeur(){
        List<ActeurDTO> liste = new ArrayList<>();
        try {
            Statement instr =
                    ConnectCinema.getInstance().createStatement();
            ResultSet rs = instr.executeQuery(SQLFindAllActeur);

            while (rs.next()) {
                int nacteur = rs.getInt(1);
                String nom = rs.getString(2);
                String prenom = rs.getString(3);

                liste.add(new ActeurDTO(nacteur, nom, prenom));
            }
        } catch (SQLException e) {
            //TODO Auto-generated catch block
            e.printStackTrace();
        }
        return liste;
    }

    public List<FilmDTO> ensfilmOfActeur(int nActeur){
        List<FilmDTO> liste = new ArrayList<>();
        try {
            PreparedStatement pstr =
                    ConnectCinema.getInstance().prepareStatement(SQLFindFilmsOfActeur);
            pstr.setInt(1,nActeur);
            ResultSet rs = pstr.executeQuery();
            while (rs.next()) {
                int nfilm = rs.getInt(1);
                String titre = rs.getString(2);
                String realisateur = rs.getString(3);
                String nature = rs.getString(4);

                liste.add(new FilmDTO(nfilm, titre, realisateur, nature));
            }
        } catch (SQLException e) {
            //TODO Auto-generated catch block
            e.printStackTrace();
        }
        return liste;
    }

    public ActeurDTO findActeurById(int nActeur) {
        ActeurDTO acteur = null;
        try {
            Connection conn = ConnectCinema.getInstance();
            PreparedStatement pstm = conn.prepareStatement(SQLFindActeurById);
            pstm.setInt(1, nActeur);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {

                String nom = rs.getString(1);
                String prenom = rs.getString(2);


                acteur = new ActeurDTO(nActeur, nom, prenom);
            }
        } catch (
                SQLException e) {
            //TODO Auto-generated catch block
            e.printStackTrace();
        }
        return acteur;
    }




    public static void main(String[] args) {

    }
}
