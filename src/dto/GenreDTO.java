package dto;

import java.io.Serializable;

// DTO EN LECTURE SEULE
public class GenreDTO implements Serializable{

    private static final long serialVersionUID = 1L;

    private int ngenre;
    private String nature;

    public int getNgenre() {
        return ngenre;
    }

    public String getNature() {
        return nature;
    }

    public GenreDTO(int ngenre, String nature){
        super();
        this.ngenre = ngenre;
        this.nature = nature;
    }

    @Override
    public String toString() {
        return nature;
    }
}
