package dto;

import java.io.Serializable;

public class ActeurDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private int  nActeur;
    private String nom;
    private String prenom;

    public ActeurDTO(int nActeur, String nom, String prenom) {
        this.nActeur = nActeur;
        this.nom = nom;
        this.prenom = prenom;
    }

    public int getnActeur() {
        return nActeur;
    }

    public void setnActeur(int nActeur) {
        this.nActeur = nActeur;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    @Override
    public String toString() {
        return "\n" +
                "nActeur=" + nActeur +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                "\n";
    }
}
