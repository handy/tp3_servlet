package dto;

import java.io.Serializable;

public class FilmDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private int nfilm;
    private String titre;
    private String nom_realisateur;
    private String genre;
    private int entrees;
    private String sortie;
    private String pays;

    public FilmDTO(int nfilm, String titre, String realisateur, String nature) {
        this.nfilm = nfilm;
        this.titre = titre ;
        this.nom_realisateur = realisateur;
        this.genre = nature;
    }

    public int getNfilm() {
        return nfilm;
    }
    public String getTitre() {
        return titre;
    }
    public String getNom_realisateur() {
        return nom_realisateur;
    }
    public String getGenre() {
        return genre;
    }
    public int getEntrees() {
        return entrees;
    }
    public String getSortie() {
        return sortie;
    }
    public String getPays() {
        return pays;
    }

    public FilmDTO(int nfilm, String titre, String nom_realisateur, String genre, int entrees, String sortie, String pays) {
        this.nfilm = nfilm;
        this.titre = titre;
        this.nom_realisateur = nom_realisateur;
        this.genre = genre;
        this.entrees = entrees;
        this.sortie = sortie;
        this.pays = pays;
    }

    public FilmDTO(int nfilm, String titre, String nom_realisateur) {
        this.nfilm = nfilm;
        this.titre = titre;
        this.nom_realisateur = nom_realisateur;
    }

    @Override
    public String toString() {
        return  titre;

    }
}
