package service;



import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectCinema {

    //URL de connexion
    private final static String URL = "jdbc:mysql://localhost:3306/myCinema";
    //Nom du USER
    private final static String USER = "userpop";
    //Mot de passe
    private final static String PW = "userpop";

    //SINGLETON
    private static Connection INSTANCE;

    //Constructeur privé
    private ConnectCinema(){
        try {
            INSTANCE = DriverManager.getConnection(URL,USER,PW);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static Connection getInstance(){
        if (INSTANCE == null){
            new ConnectCinema();
        }
        return INSTANCE;
    }

    public static void main(String[] args) {
        ConnectCinema.getInstance();
    }
}
