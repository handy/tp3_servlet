package service;

import dao.CinemaDAO;
import dto.FilmDTO;
import dto.GenreDTO;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

@WebServlet("/process")
public class ControllerServlet extends HttpServlet {

    private final static long serialVersionUID = 1L;
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try{
            String action = request.getParameter("action");
            //Execution du traitement associe a cette actin
            switch (action) {
                case "init":
                    doInit(request, response);
                    break;
                case "filmsDuGenre":
                    doFilmDuGenre(request, response);
                    break;
                case "afficherFilmsSelectionnes":
                    doFilmsSelectionnes(request, response);
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void forward(String url, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = request.getRequestDispatcher(url);
        requestDispatcher.forward(request, response);
    }

    private void doInit(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CinemaDAO cinema = CinemaDAO.getInstance();

        List<GenreDTO> ensGenres = cinema.ensGenres();

        request.setAttribute("ensGenres", ensGenres);

        String url = "pages/afficherGenres.jsp";
        forward(url, request, response);
    }

    private void doFilmDuGenre(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String choix = request.getParameter("genre");
        int ngenre = Integer.parseInt(choix);

        CinemaDAO cinema = CinemaDAO.getInstance();
        List<FilmDTO> ensFilms = cinema.ensFilmDuGenre(ngenre);
        GenreDTO genre = cinema.findGenreById(ngenre);
        request.setAttribute("ensFilms", ensFilms);



        String url = "pages/afficherFilmsDuGenre.jsp";
        forward(url, request, response);
    }

    private void doFilmsSelectionnes(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        String[] choix = request.getParameterValues("choix");

        CinemaDAO cinema = CinemaDAO.getInstance();
        HttpSession session = request.getSession();
        Map<String, FilmDTO> ensChoisis = (Hashtable<String, FilmDTO>)session.getAttribute("ensChoix");

        if (ensChoisis == null){
            ensChoisis = new HashMap<>();
        }

        for (String cle : choix){
            int nfilm = Integer.parseInt(cle);
            FilmDTO film = cinema.infoRealisateurEtActeur(nfilm);
            ensChoisis.put(cle, film);
        }

        synchronized (session){
            session.setAttribute("ensChoix", ensChoisis);

            String url = "pages/afficherFilmsChoisis.jsp";
            forward(url, request,response);
        }

    }

}
