<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8"%>
<html>
<head>
    <title>Title</title>
</head>
<body>
<h1>Votre selection :</h1>

    <c:forEach var="choix" items="${ensChoix}">
        <h3>${choix.value.titre}</h3>
        <p>realisateur : ${choix.value.nom_realisateur} - acteur principal : ${choix.value.nom_acteur}</p>
        <br>
    </c:forEach>

<a href="process?action=init">continuer les achats</a>
</body>
</html>
