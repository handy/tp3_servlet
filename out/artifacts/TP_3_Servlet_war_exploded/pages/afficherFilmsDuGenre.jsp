<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<h1>Ensemble des films</h1>
<form method="post"  action="process?action=afficherFilmsSelectionnes">
    <c:forEach var="film" items="${ensFilms}">
        <tr>

            <td>${film.titre}</td>
            <td>${film.nom_realisateur}</td>
            <td>${film.nom_acteur}</td>
            <td><input type="checkbox" name="choix" value="${film.nfilm}"></td>
            <br>

            <td></td>

        </tr>

    </c:forEach>
    <input type="submit" value="buy">
</form>
</body>
</html>
